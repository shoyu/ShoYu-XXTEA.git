[TOC]
#### 目录结构
本插件使用了火星的代码结构与方式。
引用：https://ext.dcloud.net.cn/plugin?id=596


* base64.vue  Base64加密脚本
* xxtea.vue  XXTEA加密脚本

##### 使用方法

> index.vue

```
<script>
import '@/components/shoyu-xxtea/shoyu-xxtea';
export default {
	data() {
		return {
			title: 'XXTEA',
			host: 'edusoho.taobao.com',
			nickname: 'shoyu',
			password: '123456',
			basic: false,
			xxtea: false
		};
	},
	onLoad() {
		let that = this;

		let string = that.nickname + ':' + that.password;
		that.basic = Base64.encode(string);

		that.xxtea = XXTEA.encryptToBase64(this.password, that.host);
	},
	methods: {}
};
</script>
```
